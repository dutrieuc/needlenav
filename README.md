# bikerod

Bikerod is an open-source bike GPS system based on arduino hardware with an emphasis on power saving.

## Dependancy

- Adafruit NeoPixel
- Adafruit GPS Library
- Adafruit BNO055


## Setup

Calibrate the BNO055 using restore_offset from Adafruit BNO055 library
