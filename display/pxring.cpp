#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define DELAYVAL 100

class PxRing {
    // Parameter 1 = number of pixels in strip
    // Parameter 2 = Arduino pin number (most are valid)
    // Parameter 3 = pixel type flags, add together as needed:
    //   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
    //   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
    //   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
    //   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
    //   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
  private:
    Adafruit_NeoPixel pixels;
  public:
    PxRing(uint16_t numpixels, uint16_t pin)
     : pixels(Adafruit_NeoPixel(numpixels, pin, NEO_GRB + NEO_KHZ800))
    {
    }
    
    void pxring_setup() {
      pixels.begin();
      pixels.show();
      for (int i = 0; i < pixels.numPixels(); i++) {

        pixels.setPixelColor(i, pixels.Color(0, 3, 0));
        pixels.show();
        delay(DELAYVAL);
      }
    }

    void pxring_loop() {
      pixels.clear();
      pixels.show();
    }
};
