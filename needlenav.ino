#include "display/pxring.cpp"

#define PIN 6
#define NUMPIXELS 16

PxRing pxring = PxRing (NUMPIXELS, PIN);

void setup() {
  pxring.pxring_setup();
}

void loop() {
  pxring.pxring_loop(); 
}
