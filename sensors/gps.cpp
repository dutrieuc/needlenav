#include "gps.h"

#define GPSECHO  false
Gps::Gps(HardwareSerial &ms)
    : mySerial(ms), AFgps(&mySerial), timer(millis())
{
}

void Gps::setup()
{
    delay(2000);

    // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
    AFgps.begin(9600);

    // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
    AFgps.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
    // uncomment this line to turn on only the "minimum recommended" data
    //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
    // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
    // the parser doesn't care about other sentences at this time

    // Set the update rate
    AFgps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
    // For the parsing code to work nicely and have time to sort thru the data, and
    // print it out we don't suggest using anything higher than 1 Hz

    // Request updates on antenna status, comment out to keep quiet
    AFgps.sendCommand(PGCMD_ANTENNA);

    delay(1000);
    // Ask for firmware version
    mySerial.println(PMTK_Q_RELEASE);
}

void Gps::loop()                     // run over and over again
{
    char c = AFgps.read();
    // if you want to debug, this is a good time to do it!
    if ((c) && (GPSECHO))
        Serial.write(c);

    // if a sentence is received, we can check the checksum, parse it...
    if (AFgps.newNMEAreceived()) {
        // a tricky thing here is if we print the NMEA sentence, or data
        // we end up not listening and catching other sentences!
        // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
        //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false

        if (!AFgps.parse(AFgps.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
            return;  // we can fail to parse a sentence in which case we should just wait for another
    }
}

Adafruit_GPS Gps::getAFgps(){
  return AFgps;
}
