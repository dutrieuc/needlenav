#include "imu.h"
#include <EEPROM.h>

#define BNO055_SAMPLERATE_DELAY_MS (100)     // Delay between data requests


void Imu::readCalibrationData(){
    int eeAddress = 0;
    long bnoID;
    bool foundCalib = false;

    EEPROM.get(eeAddress, bnoID);

    adafruit_bno055_offsets_t calibrationData;
    sensor_t sensor;

    /*
     *  Look for the sensor's unique ID at the beginning oF EEPROM.
     *  This isn't foolproof, but it's better than nothing.
     */
    bno.getSensor(&sensor);
    if (bnoID != sensor.sensor_id)
    {
        debugserial.println("\n[IMU] No Calibration found");
        delay(500);
    }
    else
    {
        debugserial.println("\n[IMU] Found calibration in EEPROM.");
        eeAddress += sizeof(long);
        EEPROM.get(eeAddress, calibrationData);

        debugserial.println("\n\n[IMU] Restoring calibration");
        bno.setSensorOffsets(calibrationData);

        debugserial.println("\n\n[IMU] Calibration loaded");
        foundCalib = true;
    }

    delay(1000);
}

Imu::Imu(uint16_t id, unsigned char adress)
    : bno(Adafruit_BNO055(id, adress))
{
}

void Imu::setup(void){
    if(!bno.begin())                // Initialize sensor communication
    {
        debugserial.print("no BNO055 detected");
    }
    delay(1000);
    /* Read calibration BEFORE setting up crystal */
    readCalibrationData();                
    bno.setExtCrystalUse(true);           // Use the crystal on the development board

}

void Imu::loop(void)
{
}

Adafruit_BNO055 Imu::getAFimu()
{
  return bno;
}
