#include "location.h"

Location::Location(){}

void Location::setup() {
  _imu.setup();
  _gps.setup();
}

void Location::loop() {
  _gps.loop();
  _imu.loop();
}

Gps Location::getGps() {
  return _gps;
}

Imu Location::getImu() {
  return _imu;
}
