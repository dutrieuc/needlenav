#include "location.h"
#include "mysettings.h"

#define LOG_IMU_DELAY 2000
#define SAMPLE_IMU_DELAY 10
#define RAD_2_DEG 57.295779513 //trig functions require radians, BNO055 outputs degrees
#define KN_2_MS 0.514444444

Location location = Location();
unsigned long  GPS_DELAY = 30000;
unsigned long timerImuLog;
unsigned long timerGpsLog;
unsigned long timerImuSample;
double xPos = 0, yPos = 0, estimated_speed = 0;
double last_lat = 0, last_lon = 0, magvar;

void setup() {
  logserial.begin(9600);              // Begin serial port communication
  debugserial.begin(9600);              // Begin serial port communication
  location.setup();
  timerGpsLog = -GPS_DELAY;
  timerImuLog = millis();
  timerImuSample = 0;
  gpx_header();
}

void loop() {
  location.loop();
  if (millis() - timerImuSample > SAMPLE_IMU_DELAY){
    sample_imu();
  }
  Adafruit_GPS AFgps = location.getGps().getAFgps();
  if(!AFgps.fix){
    return;
  }
  if (millis() - timerGpsLog > GPS_DELAY) {
    last_lat = AFgps.latitudeDegrees;
    last_lon = AFgps.longitudeDegrees;
    estimated_speed = AFgps.speed * KN_2_MS;
    magvar = AFgps.magvariation;
    xPos = 0;
    yPos = 0;
    log_gps();
    timerGpsLog = millis(); // reset the timer
    timerImuLog = millis();
  }
  if (millis() - timerImuLog > LOG_IMU_DELAY){
    log_imu();
    timerImuLog = millis(); // reset the timer
  }
}

void log_gps(){
  Adafruit_GPS AFgps = location.getGps().getAFgps();
  gpx_point(
    AFgps.latitudeDegrees,
    AFgps.longitudeDegrees
  );
  log_hour();
  log_speed(AFgps.speed * KN_2_MS);
  close_gpx_point();
}

void log_hour(){
  Adafruit_GPS AFgps = location.getGps().getAFgps();
  logserial.print("<time>");
  logserial.print("20");  logserial.print(AFgps.year, DEC);
  logserial.print('-');
  logserial.print(AFgps.month, DEC);   logserial.print('-');
  logserial.print(AFgps.day, DEC); logserial.print('T');
  if (AFgps.hour < 10) { logserial.print('0'); }
  logserial.print(AFgps.hour, DEC); logserial.print(':');
  if (AFgps.minute < 10) { logserial.print('0'); }
  logserial.print(AFgps.minute, DEC); logserial.print(':');
  if (AFgps.seconds < 10) { logserial.print('0'); }
  logserial.print(AFgps.seconds, DEC); logserial.print('.');
  if (AFgps.milliseconds < 10) {
      logserial.print("00");
  } else if (AFgps.milliseconds > 9 && AFgps.milliseconds < 100) {
      logserial.print("0");
  }
  logserial.print(AFgps.milliseconds);
  logserial.println("</time>");
}

void gpx_point(
  double dr_lat,
  double dr_lon){
  logserial.print("<trkpt lat=\"");
  logserial.print(dr_lat, 8);
  logserial.print("\" lon=\"" );
  logserial.print(dr_lon, 8);
  logserial.println("\">");
}

void log_speed(double speed){
  logserial.print("<speed>");
  logserial.print(speed, 8);
  logserial.println("</speed>");
}

void log_angle(double angle){
  logserial.print("<angle>");
  logserial.print(angle, 8);
  logserial.println("</angle>");
}

void log_magvariation(double magvar){
  logserial.print("<magvariation>");
  logserial.print(magvar, 8);
  logserial.println("</magvariation>");
}

void close_gpx_point(){
  logserial.println("</trkpt>");
}

void log_imu() {
  double r = 6371000.0; // add altitude
  Adafruit_BNO055 AFimu = location.getImu().getAFimu();
  sensors_event_t orientationData;
  AFimu.getEvent(&orientationData, Adafruit_BNO055::VECTOR_EULER);
  logserial.println(orientationData.orientation.x + magvar);
  logserial.println(magvar);
  logserial.println(estimated_speed * 0.36);
  gpx_point(
    last_lat + asin(yPos/r) * RAD_2_DEG,
    last_lon + asin(xPos/r) * RAD_2_DEG
  );
  close_gpx_point();
}

void sample_imu() {
  Adafruit_BNO055 AFimu = location.getImu().getAFimu();
  sensors_event_t orientationData , linearAccelData;
  AFimu.getEvent(&linearAccelData, Adafruit_BNO055::VECTOR_LINEARACCEL);
  AFimu.getEvent(&orientationData, Adafruit_BNO055::VECTOR_EULER);
  double ACCEL_VEL_TRANSITION =  (double)(millis() - timerImuSample) / 1000.0;
  //estimated_speed = estimated_speed + linearAccelData.acceleration.x * ACCEL_VEL_TRANSITION;
  
  xPos = xPos + estimated_speed * cos((orientationData.orientation.x + magvar)/ RAD_2_DEG ) * ACCEL_VEL_TRANSITION;
  yPos = yPos - estimated_speed * sin((orientationData.orientation.x + magvar)/ RAD_2_DEG ) * ACCEL_VEL_TRANSITION;
  timerImuSample = millis();
}

void gpx_header(){
  logserial.println("<gpx>");
  logserial.println("<trk>");
  logserial.println("<trkseg>");
}
